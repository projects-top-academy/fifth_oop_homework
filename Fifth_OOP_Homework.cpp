﻿/*Напишите программу, которая может менять размер двумерного динамического массива в процессе работы
1. Пользователь задаёт начальные размеры массива
2. Программа выводит его на экран(инициализируем произвольными значениями, т.е.любыми)
3. Программа задаёт вопрос, хотим ли мы изменить размер массива, при положительном ответе, по
тем же указателям выделяется память под новый массив(предварительно память нужно очистить и указатели обнулить)

Доработать задание:
1. элементы массива инициализируются случайным образом, или по значениям от пользователя
2. обеспечить копирование элементов старого массива в новый при изменении размеров
(учесть, что в случае изменения массива по размерам в меньшую сторону, необходимо предупредить пользователя, что часть данных он потеряет)
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

void printArray(int** arr, int rows, int cols) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void initializeArray(int** arr, int rows, int cols, bool userValues) {
    if (userValues) {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                std::cout << "Enter the value for element [" << i << "][" << j << "]: ";
                std::cin >> arr[i][j];
            }
        }
    }
    else {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                arr[i][j] = rand() % 100;
            }
        }
    }
}

void copyArray(int** src, int** dest, int srcRows, int srcCols, int destRows, int destCols) {
    int minRows = std::min(srcRows, destRows);
    int minCols = std::min(srcCols, destCols);

    for (int i = 0; i < minRows; ++i) {
        for (int j = 0; j < minCols; ++j) {
            dest[i][j] = src[i][j];
        }
    }
}

int main() {
    srand(time(nullptr));

    int rows, cols;

    std::cout << "Enter the initial number of rows: ";
    std::cin >> rows;

    std::cout << "Enter the initial number of columns: ";
    std::cin >> cols;

    int** arr = new int* [rows];
    for (int i = 0; i < rows; ++i) {
        arr[i] = new int[cols];
    }

    initializeArray(arr, rows, cols, false);

    printArray(arr, rows, cols);

    char choice;
    std::cout << "Do you want to resize the array? (y/n): ";
    std::cin >> choice;

    if (choice == 'y') {
        int oldRows = rows;
        int oldCols = cols;

        std::cout << "Enter the new number of rows: ";
        std::cin >> rows;

        std::cout << "Enter the new number of columns: ";
        std::cin >> cols;

        if (rows < oldRows || cols < oldCols) {
            std::cout << "Warning: Resizing the array to smaller dimensions may lead to data loss.\n";
        }

        int** newArr = new int* [rows];
        for (int i = 0; i < rows; ++i) {
            newArr[i] = new int[cols];
        }

        copyArray(arr, newArr, oldRows, oldCols, rows, cols);

        for (int i = 0; i < oldRows; ++i) {
            delete[] arr[i];
        }
        delete[] arr;

        arr = newArr;

        initializeArray(arr, rows, cols, false);

        printArray(arr, rows, cols);
    }

    for (int i = 0; i < rows; ++i) {
        delete[] arr[i];
    }
    delete[] arr;

    return 0;
}